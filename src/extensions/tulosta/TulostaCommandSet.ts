import { override } from "@microsoft/decorators";
import { Log, SPEvent } from "@microsoft/sp-core-library";
import { SPComponentLoader } from "@microsoft/sp-loader";
import {SPHttpClient, SPHttpClientResponse, ISPHttpClientOptions} from "@microsoft/sp-http";
import {
  BaseListViewCommandSet,
  Command,
  IListViewCommandSetListViewUpdatedParameters,
  IListViewCommandSetExecuteEventParameters
} from "@microsoft/sp-listview-extensibility";
import { Dialog } from "@microsoft/sp-dialog";

import * as strings from "TulostaCommandSetStrings";

/**
 * If your command set uses the ClientSideComponentProperties JSON input,
 * it will be deserialized into the BaseExtension.properties object.
 * You can define an interface to describe it.
 */
export interface ITulostaCommandSetProperties {
  // his is an example; replace with your own properties
  sampleTextOne: string;
  sampleTextTwo: string;
}

const LOG_SOURCE: string = "TulostaCommandSet";

export default class TulostaCommandSet extends BaseListViewCommandSet<ITulostaCommandSetProperties> {

  @override
  public onInit(): Promise<void> {
    Log.info(LOG_SOURCE, "Initialized TulostaCommandSet");
    return Promise.resolve();
  }

  @override
  public onListViewUpdated(event: IListViewCommandSetListViewUpdatedParameters): void {
    const compareOneCommand: Command = this.tryGetCommand("COMMAND_1");
    if (compareOneCommand) {
      // this command should be hidden unless exactly one row is selected.
      compareOneCommand.visible = event.selectedRows.length === 1;
    }
  }

  @override
  public onExecute(event: IListViewCommandSetExecuteEventParameters): void {
    switch (event.itemId) {
      case "COMMAND_1":
      var url: string = this.context.pageContext.web.absoluteUrl;
      var ID: string = event.selectedRows[0].getValueByName("ID");
      var win: Window; win =
      window.open( url + `/Lists/poikkeustilanneilmoitus/DispForm.aspx?ID=${ID}`);
      setTimeout(() => {
        var divs: NodeListOf<HTMLElement>; divs = win.document.getElementsByTagName("div");
        var div: HTMLElement;
        var i: number; i = divs.length;
        while (i--) {
          div = divs[i];

          if (div.getAttribute("role") === ("heading")) {
            div.parentNode.removeChild(div);
          }
        }
        var a: HTMLElement = <HTMLElement>win.document.querySelector(".od-UserFeedback-button");
        if(a !== null && a !== undefined) {
        a.setAttribute("style", "display: none;");
      }
        win.print();
      }, 5000);
    }
    }
  }