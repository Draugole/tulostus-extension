declare interface ITulostaCommandSetStrings {
  Command1: string;
  Command2: string;
}

declare module 'TulostaCommandSetStrings' {
  const strings: ITulostaCommandSetStrings;
  export = strings;
}
"///"